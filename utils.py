def takeOpticalPicture():
    response = cam.Streaming.channels[102].picture(
        method='get', type='opaque_data')
    with open('screenOptical.jpg', 'wb') as f:
        for chunk in response.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)


def takeThermalPicture():
    response = cam.Streaming.channels[202].picture(
        method='get', type='opaque_data')
    with open('screenThermal.jpg', 'wb') as f:
        for chunk in response.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
