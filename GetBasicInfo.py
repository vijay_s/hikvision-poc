from hikvisionapi import Client

host = ''  # IP Address Of Camera Ex : http://192.168.1.64
username = ''  # Username Ex : admin
password = ''  # Password  Ex : password

'''
Initialize Connection
'''
cam = Client(host, username, password)

'''
Get Device Information From Camera
'''
response = cam.System.deviceInfo(method='get')

print(response)
