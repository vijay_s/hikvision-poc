from hikvisionapi import Client
from datetime import datetime
from utils import takeThermalPicture, takeOpticalPicture

host = ''  # IP Address Of Camera Ex : http://192.168.1.64
username = ''  # Username Ex : admin
password = ''  # Password  Ex : password

cam = Client(host, username, password, timeout=2)

while True:
    try:
        response = cam.Event.notification.alertStream(
            method='get', type='stream')
        if response:
            # print(response)
            for packet in response:
                '''
                    Only Checking For Events Not videoloss since camera isnt configured for 
                    any event events
                '''
                if packet['EventNotificationAlert']['eventType'] != 'videoloss':
                    print(response)
                    takeThermalPicture()
                    takeOpticalPicture()
    except Exception:
        pass
